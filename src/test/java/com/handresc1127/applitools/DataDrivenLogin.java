package com.handresc1127.applitools;

import static com.google.common.base.Strings.isNullOrEmpty;
import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.EyesRunner;
import com.applitools.eyes.selenium.ClassicRunner;
import com.applitools.eyes.selenium.Eyes;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

@RunWith(DataProviderRunner.class)
public class DataDrivenLogin {

	private static EyesRunner runner;
	private static Eyes eyes;
	private static BatchInfo batch;
	private static WebDriver driver;

	@BeforeClass
	public static void setBatch() {
		batch = new BatchInfo("Data Driven Login");
		runner = new ClassicRunner();
		eyes = new Eyes(runner);
		if (isNullOrEmpty(System.getenv("APPLITOOLS_API_KEY"))) {
			throw new RuntimeException("No API Key found; Please set environment variable 'APPLITOOLS_API_KEY'.");
		}
	}

	@Before
	public void beforeEach() {
		eyes.setApiKey(System.getenv("APPLITOOLS_API_KEY"));
		eyes.setBatch(batch);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		driver.get("https://www.linkedin.com/login");

	}
	
	@DataProvider
    public static Object[][] data() {
		return new Object[][] { 
					{ "", 					"", 		"Please enter an email address or phone number", 	"Prueba 1. Sin user ni pass" },
					{ "Usuario", 			"", 		"Please enter a valid username", 					"Prueba 2. User invalid without pass" },
					{ "", 					"Passw0rd", "Please enter an email address or phone number", 	"Prueba 3. Sin user" },
					{ "Usuario", 			"Passw0rd", "Please enter a valid username", 					"Prueba 4. User invalid with pass" },
					{ "Usuario@gmail.com", 	"Passw0rd", "Ok", 												"Prueba 5. All ok" }, 
			};
	}


	public void login(String user, String pass, String testName) {
		eyes.open(driver, "Data Driven Login", testName);
		driver.findElement(By.id("username")).sendKeys(user);
		driver.findElement(By.id("password")).sendKeys(pass);
		driver.findElement(By.xpath("//button")).click();
		//eyes.check(testName, Target.window().ignoreDisplacements());
		eyes.checkWindow(testName);
		eyes.closeAsync();
	}

    @Test
    @UseDataProvider("data")
	public void testLogin(String username, String password, String expected, String testName) {
    	System.out.println("User:"+username+" Pass:"+password+" testName:"+testName+" Expected:"+expected);
		login(username,password, testName);
		String dataActual="Ok";
		if(!testName.contains("All ok")) {
			dataActual = driver.findElement(By.id("error-for-username")).getText();
		}
		assertEquals(expected, dataActual);
	}
	
	@After
	public void afterEach() {
		driver.quit();
		eyes.abortIfNotClosed();
	}
}
