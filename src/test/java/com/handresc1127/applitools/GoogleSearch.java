package com.handresc1127.applitools;

import static com.google.common.base.Strings.isNullOrEmpty;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.EyesRunner;
import com.applitools.eyes.selenium.ClassicRunner;
import com.applitools.eyes.selenium.Eyes;

public class GoogleSearch {
	private static EyesRunner runner;
	private static BatchInfo batch;
	private static Eyes eyes;
	private static WebDriver driver;

	@BeforeClass
	public static void setBatch() {
		// Must be before ALL tests (at Class-level)
		batch = new BatchInfo("Google Search");
	}

	@Before
	public void beforeEach() {
		// Initialize the Runner for your test.
		runner = new ClassicRunner();

		// Initialize the eyes SDK
		eyes = new Eyes(runner);

		// Raise an error if no API Key has been found.
		if (isNullOrEmpty(System.getenv("APPLITOOLS_API_KEY"))) {
			throw new RuntimeException("No API Key found; Please set environment variable 'APPLITOOLS_API_KEY'.");
		}

		// Set your personal Applitols API Key from your environment variables.
		eyes.setApiKey(System.getenv("APPLITOOLS_API_KEY"));

		// set batch name
		eyes.setBatch(batch);

		// Use Chrome browser
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);

	}

	@Test
	public void basicTest() {
		// Set AUT's name, test name and viewport size (width X height)
		// We have set it to 800 x 600 to accommodate various screens. Feel free to
		// change it.
		eyes.open(driver, "Demo App", "Google Search");

		// Navigate the browser to the "ACME" demo app.
		driver.get("https://www.google.com");
		eyes.checkWindow("Google");
		
		driver.findElement(By.name("q")).clear();
		driver.findElement(By.name("q")).sendKeys("Henry Andres Correa Correa");
		driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
		eyes.checkWindow("Google");
		
		driver.findElement(By.xpath("//*[@id='hdtb-msb-vis']//a[contains(.,'Imágenes')]")).click();
		eyes.checkWindow("Google");
		
		eyes.closeAsync();
	}

	@After
	public void afterEach() {
		// Close the browser.
		driver.quit();
		eyes.abortIfNotClosed();
	}

}
