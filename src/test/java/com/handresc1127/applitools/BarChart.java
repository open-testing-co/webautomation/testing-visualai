package com.handresc1127.applitools;

import static com.google.common.base.Strings.isNullOrEmpty;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.EyesRunner;
import com.applitools.eyes.selenium.ClassicRunner;
import com.applitools.eyes.selenium.Eyes;

@RunWith(JUnit4.class)
public class BarChart {

	private static EyesRunner runner;
	private static Eyes eyes;
	private static BatchInfo batch;
	private static WebDriver driver;

	@BeforeClass
	public static void setBatch() {
		batch = new BatchInfo("Bar Chart");
		runner = new ClassicRunner();
		eyes = new Eyes(runner);
		if (isNullOrEmpty(System.getenv("APPLITOOLS_API_KEY"))) {
			throw new RuntimeException("No API Key found; Please set environment variable 'APPLITOOLS_API_KEY'.");
		}
	}

	@Before
	public void beforeEach() {
		eyes.setApiKey(System.getenv("APPLITOOLS_API_KEY"));
		eyes.setBatch(batch);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);
		driver.get("https://demo.applitools.com/BarChart.html");
	}

    @Test
	public void chartTest1() {
    	eyes.open(driver, "Bar Chart", "ChartTest 1");
		eyes.checkWindow("1st test window");
		eyes.closeAsync();
	}
    
    @Test
	public void chartTest2() {
		eyes.open(driver, "Bar Chart", "ChartTest 2");
		driver.findElement(By.id("addDataset")).click();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {}
		eyes.checkWindow("2nd test window");
		eyes.closeAsync();	
	}
    
    @Test
	public void chartTest3() {
		eyes.open(driver, "Bar Chart", "ChartTest 2");
		eyes.checkWindow("3nd test window");
		driver.findElement(By.id("addDataset")).click();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {}
		eyes.checkWindow("3nd test window");
		eyes.closeAsync();	
	}
	
	@After
	public void afterEach() {
		driver.quit();
		eyes.abortIfNotClosed();
	}
}
